import Vue from 'vue'
import Vuex from 'vuex'
import vuexI18n from 'vuex-i18n'
import i18n from 'data/i18n.coffee'

import sensorTypes from 'data/sensorTypes.coffee'
import {defaultDevice} from 'data/devices.coffee'

export store = undefined
export init = () ->
	store = new Vuex.Store({
		state: {
			experiment: {
				sensors: []
			}
		}
		mutations: {
			addSensor: (state, sensorTypeIndex) ->
				sensor = sensorTypes[sensorTypeIndex]
				state.experiment.sensors.push({
					type: sensorTypeIndex
					name: "New " + sensor.name
					desc: ""
					device: defaultDevice
				})
			removeSensor: (state, sensorIndex) ->
				state.experiment.sensors.splice(sensorIndex, 1)
			changeExperimentDetails: (state, details) ->
				Object.assign(state.experiment, details)
		}
	})
	Vue.use(vuexI18n.plugin, store)
	for lang of i18n
		Vue.i18n.add(lang, i18n[lang])
	Vue.i18n.set("en-us")