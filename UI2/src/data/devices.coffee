export default [
	{
		id: "sb_poc_1"
		name: "Sensorboard POC Rev1"
		analogInputs: [8, 9, 10, 11, 12, 13]
		analogOutputs: [2, 4, 5, 8]
		digitalInputs: [1, 2, 3, 4, 5, 6, 7]
		digitalOutputs: [1, 2, 3, 4, 5, 6, 7]
	}
]

export defaultDevice = "sb_poc_1"