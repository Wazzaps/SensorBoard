export default {
	"en-us": {
		# All default
	}
	"he-il": {
		"Edit project details": "ערוך פרטי פרוייקט"
		"Close project": "סגור פרוייקט"
		"Not connected": "לא מחובר"
		"Connected": "מחובר"
		"Settings": "הגדרות"

		"Devices": "מכשירים"

		"Project Details": "פרטי הפרוייקט"
		"Project name": "שם הפרוייקט"
		"Project description": "תיאור הפרוייקט"
		"Device": "מכשיר"
	}
}