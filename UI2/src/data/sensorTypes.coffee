export default [
	{
		id: "ain"
		name: "Analog input"
		desc: "Reads the voltage of a pin"
		icon: "ion-log-in"
		analogInputs: ["The Input pin"]
		analogOutputs: []
		digitalInputs: []
		digitalOutputs: []
	}
	{
		id: "din"
		name: "Digital input"
		desc: "Reads digital state of a pin"
		icon: "ion-log-in"
		analogInputs: []
		analogOutputs: []
		digitalInputs: ["The Input pin"]
		digitalOutputs: []
	}
	{
		id: "din-neutral"
		name: "Digital input (No pullup)"
		desc: "Reads digital state of a pin (Without floating voltage protection)"
		icon: "ion-log-in"
		analogInputs: []
		analogOutputs: []
		digitalInputs: ["The Input pin"]
		digitalOutputs: []
	}
]