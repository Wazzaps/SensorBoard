require("./themes/app.#{__THEME}.styl")

import Vue from 'vue'
import Vuex from 'vuex'
import {init as storeInit, store} from './store.coffee'
import Quasar from 'quasar'
import router from './router.coffee'

Vue.config.productionTip = false

Vue.use(Vuex)
Vue.use(Quasar) # Install Quasar Framework


storeInit()

if __THEME is 'mat'
  require('quasar-extras/roboto-font')

import 'quasar-extras/material-icons'
import 'quasar-extras/ionicons'
import 'quasar-extras/fontawesome'
import 'quasar-extras/animate'

Quasar.start =>
  new Vue {
    el: '#q-app'
    store: store
    router
    render: (h) -> h(require('./App').default)
  }
