import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

load = (component) =>
  return () =>
    return `import("@/" + component + ".vue")`

export default new VueRouter({
	mode: 'hash',

	routes: [
		{ path: '/', component: load('Welcome') },
		{ path: '/experiment', component: load('Experiment') },
		{ path: '*', component: load('Error404') } # Not found
	]
})
