export interface SensorVisResult {
	visAvailable: boolean,
	visValue: number,
	visPrettyValue: string
}

export interface RealtimeSensor {
	readValue: () => SensorVisResult
}