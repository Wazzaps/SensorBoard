Firmata Protocol extension
========
```c
message SET_TIME = 



enum to_message_type {
  ENTER_FIRMATA = 0
  SET_TIME = 1
  CONFIGURE_EXPERIMENT
}

enum from_message_type {
  SENSOR_READING
}

enum sensor_type {
  DIGITAL_INPUT = 0
  ANALOG_INPUT = 1
  // TODO
}

struct single_sensor { // 13 bytes
  sensor_type type // 16 bits
  uint8 analog_in_connectors[4]
  uint8 dio_connectors[4]
  uint8 i2c_address
  uint8 spi_connector // equals one always on nano
  uint8 uart_connector // equals zero always on nano
}

struct set_time {
  uint16 year
  uint8 month
  uint8 day
  uint8 hour
  uint8 min
  uint8 sec
}

struct configure_experiment { // 1-832 bytes (1-208 bytes in nano)
  single_sensor sensors[64] // up to 16 in nano
}

```