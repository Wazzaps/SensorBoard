"use strict";
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var sender;
exports.analogValues = [];
// Meta stuff
function setSender(func) {
    sender = func;
}
exports.setSender = setSender;
function packetReceiver() {
    var cmd, pin, _a, _b, _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                if (!true) return [3 /*break*/, 5];
                return [4 /*yield*/];
            case 1:
                cmd = _d.sent();
                if (!(cmd >> 5 == 0x04)) return [3 /*break*/, 3];
                pin = (cmd & 0x03) >> 2;
                //console.log("Voltage @" + pin + " = " + value);
                _a = exports.analogValues;
                _b = pin;
                _c = (cmd & 0x03);
                return [4 /*yield*/];
            case 2:
                //console.log("Voltage @" + pin + " = " + value);
                _a[_b] = _c + (_d.sent());
                return [3 /*break*/, 4];
            case 3:
                if (cmd >> 5 == 0x02) {
                }
                _d.label = 4;
            case 4: return [3 /*break*/, 0];
            case 5: return [2 /*return*/];
        }
    });
}
exports.receiver = packetReceiver();
exports.receiver.next();
// Commands
var PIN_TYPE;
(function (PIN_TYPE) {
    PIN_TYPE[PIN_TYPE["DIGITAL"] = 0] = "DIGITAL";
    PIN_TYPE[PIN_TYPE["ANALOG"] = 1] = "ANALOG";
})(PIN_TYPE = exports.PIN_TYPE || (exports.PIN_TYPE = {}));
var PIN_MODE;
(function (PIN_MODE) {
    PIN_MODE[PIN_MODE["NONE"] = 2] = "NONE";
    PIN_MODE[PIN_MODE["INPUT"] = 3] = "INPUT";
    PIN_MODE[PIN_MODE["INPUT_PULLUP"] = 0] = "INPUT_PULLUP";
    PIN_MODE[PIN_MODE["OUTPUT"] = 1] = "OUTPUT";
    PIN_MODE[PIN_MODE["OFF"] = 0] = "OFF";
    PIN_MODE[PIN_MODE["ON"] = 1] = "ON";
})(PIN_MODE = exports.PIN_MODE || (exports.PIN_MODE = {}));
function ultraSonicBegin(pingPin, echoPin, rate) {
    rate = rate || 1;
    if (pingPin < 8 && echoPin < 8) {
        sender([0x10, (pingPin << 4) + echoPin, rate - 1]); // 00 01 00 00, pp pp ee ee, rr rr rr rr
    }
    else {
        sender([0x11, pingPin, echoPin, rate - 1]); // 00 01 00 01, pp pp pp pp, ee ee ee ee, rr rr rr rr
    }
}
exports.ultraSonicBegin = ultraSonicBegin;
function pinMode(pinType, pin, pinMode) {
    pinMode = pinMode === undefined ? PIN_MODE.ON : pinMode;
    if (pin < 8 && pinMode <= 1) {
        sender([0xC0 + (pinType << 4) + (pin << 1) + pinMode]); // 11 0t pp pm
    }
    else {
        sender([0xE0 + (pinType << 4) + (pin >> 6), ((pin & 0xFC) << 2) + pinMode]); // 11 1t pp pp, pp pp pp mm
    }
}
exports.pinMode = pinMode;
function digitalWrite(pin, value) {
    if (pin < 16) {
        sender([(0x40) + (pin << 1) + (value & 0x01)]); // 01 0p pp pv
    }
    else {
        // TODO
        sender([(0x60) + (pin >> 6), (pin << 2) + (value & 0x01)]); // 01 10 pp pp, pp pp pp 0v
    }
}
exports.digitalWrite = digitalWrite;
function reset() {
    sender([0x70]);
}
exports.reset = reset;
// Variables
var ainValues = [];
ainValues.length;
//////////////////
function test() {
    //reset();
    /*setInterval(() => {
        digitalWrite(0, 1);
        setTimeout(() => {
            digitalWrite(0, 0);
        }, 500);
    }, 1000);*/
    pinMode(PIN_TYPE.ANALOG, 5, PIN_MODE.ON);
    //ultraSonicBegin(0, 1, 10);
}
exports.test = test;
