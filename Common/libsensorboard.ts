let sender: (data: number[]) => void;
export let customHandlers: any[];

export let analogValues: number[] = [];

// Meta stuff
export function setSender(func: (data: number[]) => void) {
	sender = func;
}

function* packetReceiver() {
	while (true) {
		let cmd = yield;
		//console.log(cmd >> 5);
		if (cmd >> 5 == 0x04) { // Short analog report - 10 0p pp vv, vv vv vv vv
			let pin = (cmd & 0x03) >> 2;
			let value = (cmd & 0x03) + (yield);
			console.log("Voltage @" + pin + " = " + value);
			analogValues[pin] = value;
		} else if (cmd >> 5 == 0x02) { // Short digital report

		}
	}
}

export const receiver = packetReceiver();
receiver.next();

// Commands
export enum PIN_TYPE {
	DIGITAL = 0,
	ANALOG = 1
}

export enum PIN_MODE {
	NONE = 2,
	INPUT = 3,
	INPUT_PULLUP = 0,
	OUTPUT = 1,
	OFF = 0,
	ON = 1
}

export function ultraSonicBegin(pingPin: number, echoPin: number, rate?: number) {
	rate = rate || 1;
	if (pingPin < 8 && echoPin < 8) { // Short
		sender([0x10, (pingPin << 4) + echoPin, rate - 1]); // 00 01 00 00, pp pp ee ee, rr rr rr rr
	} else { // Long
		sender([0x11, pingPin, echoPin, rate - 1]); // 00 01 00 01, pp pp pp pp, ee ee ee ee, rr rr rr rr
	}
}

export function pinMode(pinType: PIN_TYPE, pin: number, pinMode?: PIN_MODE) {
	pinMode = pinMode === undefined ? PIN_MODE.ON : pinMode;
	if (pin < 8 && pinMode <= 1) { // Short
		sender([0xC0 + (pinType << 4) + (pin << 1) + pinMode]); // 11 0t pp pm
	} else { // Long
		sender([0xE0 + (pinType << 4) + (pin >> 6), ((pin & 0xFC) << 2) + pinMode]); // 11 1t pp pp, pp pp pp mm
	}
}

export function digitalWrite(pin: number, value: number) {
	if (pin < 16) { // Short
		sender([(0x40) + (pin << 1) + (value & 0x01)]); // 01 0p pp pv
	} else { // Long
		// TODO
		sender([(0x60) + (pin >> 6), (pin << 2) + (value & 0x01)]); // 01 10 pp pp, pp pp pp 0v
	}
}

export function reset() {
	sender([0x70]);
}

// Variables
let ainValues: number[] = [];
ainValues.length;

//////////////////

export function test() {
	//reset();
	/*setInterval(() => {
		digitalWrite(0, 1);
		setTimeout(() => {
			digitalWrite(0, 0);
		}, 500);
	}, 1000);*/

	pinMode(PIN_TYPE.ANALOG, 5, PIN_MODE.ON);
	//ultraSonicBegin(0, 1, 10);
}