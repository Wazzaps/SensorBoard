#include "SBProtocol.h"
#include "structures.h"

// Parameters
#define BT_BAUD_RATE 9600
#define VERSION 0
//const byte dioPinMapping[] = {2, 3, 4, 5, 6, 7, 8};
const byte dioPinMapping[] = {13, 3, 4, 5, 6, 7, 8}; // FIXME
const byte aouPinMapping[] = {3, 5, 6};
const byte ainPinMapping[] = {7, 6, 3, 2, 1, 0};

// Runtime
byte dinReporting[sizeof(dioPinMapping)] = {};
byte ainReporting[sizeof(ainPinMapping)] = {};
byte dinLastState[sizeof(dioPinMapping)] = {};
uint16_t ainLastState[sizeof(ainPinMapping)] = {};

byte sampleDelay = 8;
uint16_t ainThreshold = 0;
unsigned long lastMicros = 0;

// Code
byte waitForByte() {
  do {
    while((micros() - lastMicros) / 1000 < sampleDelay);
    lastMicros = micros();
    
    for (int pin = 0; pin < sizeof(dioPinMapping); pin++) {
      if (dinReporting[pin]) {
        byte newState = digitalRead(dioPinMapping[pin]);
        if (dinLastState[pin] != newState) {
          if (pin < 16) { // Short digital report
            Serial.write(0x40 + (pin << 1) + newState);
          } else { // Long digital report
            Serial.write(0x60 + (pin >> 6));
            Serial.write((pin << 2) + newState);
          }
        }
      }
    }
    for (int pin = 0; pin < sizeof(ainPinMapping); pin++) {
      if (ainReporting[pin]) {
        uint16_t newState = analogRead(ainPinMapping[pin]);
        if (ainLastState[pin] != newState) {
          if (pin < 8) { // Short analog report
            Serial.write((byte)(0x80 + (pin << 2) + (newState >> 8)));
            Serial.write((byte)newState);
          } else { // Long analog report
            Serial.write((byte)(0xA0 + (pin >> 6)));
            Serial.write((byte)((pin << 2) + (newState >> 8)));
            Serial.write((byte)newState);
          }
        }
      }
    }
  } while(!Serial.available());
  //micros();
  return Serial.read();
}

void setup() {
  Serial.begin(BT_BAUD_RATE);
}

void reset_handler() {

}

void loop() {
  byte cmd = waitForByte();
  byte params[6];
  uint32_t vars[4];
  
  switch(cmd >> 6) {
    case 0:
      extended_handler(cmd);
      break;
    case 1:
      if (!(cmd & 0x20)) { // Short digital write
        digitalWrite(dioPinMapping[(cmd & 0x1F) >> 1], cmd & 0x01);
      } else if (!(cmd & 0x10)) { // Long digital write
        params[0] = waitForByte();
        digitalWrite(dioPinMapping[(cmd & 0x0F) << 6 + params[0] >> 2], params[0] & 0x01);
      } else { // System command
        system_handler(cmd);
      }
      break;
    case 2: // Analog write
      params[0] = waitForByte();
      analogWrite(aouPinMapping[cmd & 0x3F], params[0]);
      break;
    case 3:
      if (cmd & 0x20) { // Long pinmode
        params[0] = waitForByte();
        vars[0] = (cmd & 0x10) >> 4; // Type (analog/digital)
        vars[1] = (cmd & 0x0F) << 6 + params[0] >> 2; // Pin num
        vars[2] = params[0] & 0x03; // Target mode
      } else { // Short pinmode
        vars[0] = (cmd & 0x10) >> 4; // Type (analog/digital)
        vars[1] = (cmd & 0x0F) >> 1; // Pin num
        vars[2] = cmd & 0x01; // Target mode
      }
      if (vars[0]) { // Analog
        ainReporting[vars[1]] = vars[2] != 0;
      } else {  // Digital
        // INPUT_PULLUP/OUTPUT/NONE/INPUT
        if (vars[2] == 0)
          pinMode(dioPinMapping[vars[1]], INPUT_PULLUP);
        else if (vars[2] == 1)
          pinMode(dioPinMapping[vars[1]], OUTPUT);
        else if (vars[2] == 2)
          pinMode(dioPinMapping[vars[1]], INPUT);
        else if (vars[2] == 3)
          pinMode(dioPinMapping[vars[1]], INPUT);
        dinReporting[vars[1]] = vars[2] == 0 || vars[2] == 3;
      }
      break;

  }
}

void system_handler(byte cmd) {
  if ((cmd & 0x0F) >> 2 == 0 || (cmd & 0x0F) >> 2 == 3) {
    switch (cmd & 0x0F) {
      case 0: // Reset
        reset_handler();    
        break;
      case 1: // TODO: Set time

        break;
      case 2: // TODO: Upload experiment

        break;
      case 3: // TODO: Execute experiment

        break;

      case 12: // Get version
        Serial.write((uint8_t) (VERSION >> 8));
        Serial.write((uint8_t) (VERSION & 0xFF));
        break;
    }
  } else {
    if ((cmd & 0x0F) >> 2 == 1) { // Set global sample rate
      switch(cmd & 0x03) {
        case 0:
          sampleDelay = 1;
          break;
        case 1:
          sampleDelay = 2;
          break;
        case 2:
          sampleDelay = 4;
          break;
        case 3:
          sampleDelay = 8;
          break;
      }
    } else { // Set analog threshold before report
      ainThreshold = (cmd & 0x03) << 8 + waitForByte();
    }
  }
}

void extended_handler(byte cmd) {

}

void custom_handler(byte cmd) {

}