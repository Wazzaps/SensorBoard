"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var model_1 = require("./model");
var l10n = require("../../l10n/l10n");
var utils_1 = require("../../misc/utils");
function init(root) {
    return rivets.bind(root, {
        recents: model_1.recents,
        remove_recent: function (mouse_evt, state) {
            model_1.recents.splice(state.index, 1);
        },
        change_l10n: function (mouse_evt) {
            l10n.change_l10n(mouse_evt.target.getAttribute("data-to"));
        },
        newExperiment: function () {
            // TODO: Setup state
            page.show("/experiment_setup");
            utils_1.$1("#experiment-title-input").focus();
        }
    });
}
exports.init = init;
function focus(ctx) {
}
exports.focus = focus;
