export let recents = [
	{
		"title": "Testing 1",
		"path": "",
		"type": "experiment"
	},
	{
		"title": "Testing 2",
		"path": "/home/david/SensorBoard/Experiments/test2.sbe",
		"type": "experiment"
	},
	{
		"title": "Testing 3",
		"path": "/home/david/SensorBoard/Experiments/test3.sbp",
		"type": "program"
	},
	{
		"title": "Testing 4",
		"path": "/home/david/SensorBoard/Experiments/test4.sbp",
		"type": "program"
	}
];