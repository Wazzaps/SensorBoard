import {recents} from "./model";
import * as l10n from "../../l10n/l10n";
import {$1} from "../../misc/utils";

export function init(root: HTMLElement): any {
	return rivets.bind(root, {
		recents: recents,
		remove_recent: (mouse_evt, state) => {
			recents.splice(state.index, 1);
		},
		change_l10n: (mouse_evt) => {
			l10n.change_l10n(mouse_evt.target.getAttribute("data-to"));
		},
		newExperiment: () => {
			// TODO: Setup state
			page.show("/experiment_setup");
			$1("#experiment-title-input").focus();
		}
	});
}

export function focus(ctx: any) {

}