import {
	analogInputPins, analogOutputPins, defaultSensor, deviceNames, digitalInputPins,
	digitalOutputPins, sensorTypes
} from "./model";
import {$1} from "../../misc/utils";
import {globalstate} from "../../controllers/main";
import {_l10n, change_l10n} from "../../l10n/l10n";
import * as dataVis from "../../controllers/dataVis";
import * as AnalogInput from "./sensors/AnalogInput";

let expState;

export function init(root: HTMLElement): any {
	rivets.formatters.getSensorType = sensor_id => sensorTypes[sensor_id];
	expState = globalstate.experiment;

	root.addEventListener("click", (evt) => {
		if (!(evt.target as any).classList.contains("hamburger-menu-item") && (evt.target as any).id !== "experiment-menu-btn") {
			expState.state.hamMenuOpen = false;
		}
	});

	setInterval(() => {
		expState.currentExperiment.sensors.forEach(async (sensor) => {
			if (sensor.type == 1) { // Analog Input
				console.log(await AnalogInput.prettyValue(sensor));
			}
		});
	}, 2000);

	return rivets.bind(root, {
		// State
		exp: expState.currentExperiment,
		state: expState.state,
		hamMenuItems: [
			{
				name: "open",
				onclick: () => {

				}
			},
			{
				name: "save",
				onclick: () => {

				}
			},
			{
				name: "change_lang",
				onclick: () => {
					if (_l10n.current_lang == "en_us") {
						change_l10n("he_il");
					} else {
						change_l10n("en_us");
					}
				}
			},
		],

		// Static data
		deviceSelection: deviceNames,
		analogInPossiblePins: analogInputPins,
		analogOutPossiblePins: analogOutputPins,
		digitalInPossiblePins: digitalInputPins,
		digitalOutPossiblePins: digitalOutputPins,
		sensorTypes: sensorTypes,

		// Methods
		toggleExperimentDetailsView: () => {
			expState.state.detailsOpen = !expState.state.detailsOpen;
			if (expState.state.detailsOpen) {
				$1("#experiment-title-input").focus();
				if (expState.currentExperiment.title == "Empty Title") {
					expState.currentExperiment.title = "";
				}
			} else if (expState.currentExperiment.title == "") {
				expState.currentExperiment.title = "Empty Title";
			}
		},
		toggleMenu: () => {
			/*if (_l10n.current_lang == "en_us") {
				change_l10n("he_il");
			} else {
				change_l10n("en_us");
			}*/
			expState.state.hamMenuOpen = !expState.state.hamMenuOpen;
		},
		remove_sensor: (mouse_evt, state) => {
			expState.currentExperiment.sensors.splice(state.index, 1);
		},
		add_sensor: () => {
			expState.currentExperiment.sensors.push(Object.assign({}, defaultSensor, {
				type: expState.state.newSensorSelection,
				analogInputs: [0],//new Array(sensorTypes[expState.state.newSensorSelection].analogInputs.length).fill(0),
				analogOutputs: new Array(sensorTypes[expState.state.newSensorSelection].analogOutputs.length).fill(0),
				digitalInputs: new Array(sensorTypes[expState.state.newSensorSelection].digitalInputs.length).fill(0),
				digitalOutputs: new Array(sensorTypes[expState.state.newSensorSelection].digitalOutputs.length).fill(0)
			}));
		},
		updated: (evt, state) => {
			//state["%sensor%"] // Sensor number
			//state["%ain%/%aout%/%din%/%dout%"] // Pin number in sensor
			dataVis.update();
		},

		s: globalstate
	});
}

export function focus(ctx: any) {

}