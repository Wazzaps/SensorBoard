"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var model_1 = require("./model");
var utils_1 = require("../../misc/utils");
var main_1 = require("../../controllers/main");
var expState;
function init(root) {
    var _this = this;
    rivets.formatters.getSensorType = function (sensor_id) { return model_1.sensorTypes[sensor_id]; };
    expState = main_1.globalstate.experiment;
    return rivets.bind(root, {
        // State
        exp: expState.currentExperiment,
        state: expState.state,
        hamMenuItems: [{
                name: "Test1",
                onclick: function () {
                }
            }],
        hamMenuOpen: false,
        // Static data
        deviceSelection: model_1.deviceNames,
        analogInPossiblePins: model_1.analogInputPins,
        analogOutPossiblePins: model_1.analogOutputPins,
        digitalInPossiblePins: model_1.digitalInputPins,
        digitalOutPossiblePins: model_1.digitalOutputPins,
        sensorTypes: model_1.sensorTypes,
        // Methods
        toggleExperimentDetailsView: function () {
            expState.state.detailsOpen = !expState.state.detailsOpen;
            if (expState.state.detailsOpen) {
                utils_1.$1("#experiment-title-input").focus();
                if (expState.currentExperiment.title == "Empty Title") {
                    expState.currentExperiment.title = "";
                }
            }
            else if (expState.currentExperiment.title == "") {
                expState.currentExperiment.title = "Empty Title";
            }
        },
        toggleMenu: function () {
            /*if (_l10n.current_lang == "en_us") {
                change_l10n("he_il");
            } else {
                change_l10n("en_us");
            }*/
            _this.hamMenuOpen = true;
            //page.show("/experiment_setup");
        },
        remove_sensor: function (mouse_evt, state) {
            expState.currentExperiment.sensors.splice(state.index, 1);
        },
        add_sensor: function (mouse_evt, state) {
            expState.currentExperiment.sensors.push(Object.assign({}, model_1.defaultSensor, {
                type: expState.state.newSensorSelection,
                analogInputs: new Array(model_1.sensorTypes[expState.state.newSensorSelection].analogInputs.length).fill(0),
                analogOutputs: new Array(model_1.sensorTypes[expState.state.newSensorSelection].analogOutputs.length).fill(0),
                digitalInputs: new Array(model_1.sensorTypes[expState.state.newSensorSelection].digitalInputs.length).fill(0),
                digitalOutputs: new Array(model_1.sensorTypes[expState.state.newSensorSelection].digitalOutputs.length).fill(0)
            }));
        }
    });
}
exports.init = init;
function focus(ctx) {
}
exports.focus = focus;
