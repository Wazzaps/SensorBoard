"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Device;
(function (Device) {
    Device[Device["SensorBoardNanoRev1"] = 0] = "SensorBoardNanoRev1";
})(Device = exports.Device || (exports.Device = {}));
exports.deviceNames = [
    "SensorBoard Nano Rev1"
];
exports.defaultSensorType = {
    "name": "Unnamed",
    "desc": "",
    "image": "",
    analogInputs: [],
    analogOutputs: [],
    digitalInputs: [],
    digitalOutputs: [],
    I2CAddress: "",
    configurableI2CAddress: false,
    hasSPIConnector: false,
    hasUARTConnector: false
};
exports.defaultSensor = {
    type: 0,
    analogInputs: [],
    analogOutputs: [],
    digitalInputs: [],
    digitalOutputs: [],
    I2CAddress: -1,
    SPIConnector: -1,
    UARTConnector: -1,
    visValue: ""
};
exports.analogInputPins = [0, 8, 9, 10, 11, 12, 13];
exports.analogOutputPins = [0, 2, 4, 5];
exports.digitalInputPins = [0, 1, 2, 3, 4, 5, 6, 7];
exports.digitalOutputPins = [0, 1, 2, 3, 4, 5, 6, 7];
exports.sensorTypes = [
    Object.assign({}, exports.defaultSensorType, {
        "name": "digital_input",
        "desc": "Lorem ipsum",
        digitalInputs: ["the_input_pin"]
    }),
    Object.assign({}, exports.defaultSensorType, {
        "name": "analog_input",
        "desc": "Lorem ipsum",
        analogInputs: ["the_input_pin"]
    }),
    Object.assign({}, exports.defaultSensorType, {
        "name": "ultrasonic_distance_sensor",
        "desc": "Lorem ipsum",
        digitalInputs: ["the_echo_pin"],
        digitalOutputs: ["the_ping_pin"]
    })
];
exports.defaultExperiment = {
    filename: "",
    path: "",
    title: "",
    desc: "",
    device: Device.SensorBoardNanoRev1,
    sensors: []
};
