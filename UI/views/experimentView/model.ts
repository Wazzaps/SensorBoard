import * as AnalogInput from "./sensors/AnalogInput";

export enum Device {
	SensorBoardNanoRev1 = 0
}

export interface SensorType {
	name: string;
	desc: string;
	image: string;

	analogInputs: string[];
	analogOutputs: string[];
	digitalInputs: string[];
	digitalOutputs: string[];
	I2CAddress: string;
	configurableI2CAddress: boolean;
	hasSPIConnector: boolean;
	hasUARTConnector: boolean;

	initFunc: (sensor: Sensor) => void;
	valueFunc: (sensor: Sensor) => number;
	prettyValueFunc: (sensor: Sensor) => string;
}

export interface Sensor {
	type: number; // Index in "sensorTypes"
	analogInputs: number[]; // Already mapped to physical pins on the board
	analogOutputs: number[]; // Already mapped to physical pins on the board
	digitalInputs: number[]; // Already mapped to physical pins on the board
	digitalOutputs: number[]; // Already mapped to physical pins on the board
	I2CAddress: number;
	SPIConnector: number; // equals one always on nano
	UARTConnector: number; // equals zero always on nano
	visOpen: boolean;
	visValue: number;
	visPrettyValue: string;
}

export let deviceNames: string[] = [
	"SensorBoard Nano Rev1"
];

export let defaultSensorType: SensorType = {
	"name": "Unnamed",
	"desc": "",
	"image": "",
	analogInputs: [],
	analogOutputs: [],
	digitalInputs: [],
	digitalOutputs: [],
	I2CAddress: "",
	configurableI2CAddress: false,
	hasSPIConnector: false,
	hasUARTConnector: false,
	initFunc: () => console.log("UNDEFINED SENSOR INIT"),
	valueFunc: () => -1,
	prettyValueFunc: () => "UNDEFINED"
};

export let defaultSensor: Sensor = {
	type: 0, // Index in "sensorTypes"
	analogInputs: [],
	analogOutputs: [],
	digitalInputs: [],
	digitalOutputs: [],
	I2CAddress: -1,
	SPIConnector: -1, // equals one always on nano
	UARTConnector: -1, // equals zero always on nano
	visOpen: false,
	visValue: 0,
	visPrettyValue: ""
};

export let analogInputPins: number[] = [0, 8, 9, 10, 11, 12, 13];
export let analogOutputPins: number[] = [0, 2, 4, 5];
export let digitalInputPins: number[] = [0, 1, 2, 3, 4, 5, 6, 7];
export let digitalOutputPins: number[] = [0, 1, 2, 3, 4, 5, 6, 7];

export let sensorTypes: SensorType[] = [
	Object.assign({}, defaultSensorType, {
		"name": "digital_input",
		"desc": "Lorem ipsum",
		digitalInputs: ["the_input_pin"]
	}),
	Object.assign({}, defaultSensorType, {
		"name": "analog_input",
		"desc": "Lorem ipsum",
		analogInputs: ["the_input_pin"],

		initFunc: AnalogInput.init,
		valueFunc: AnalogInput.value,
		prettyValueFunc: AnalogInput.prettyValue
	}),
	Object.assign({}, defaultSensorType, {
		"name": "ultrasonic_distance_sensor",
		"desc": "Lorem ipsum",
		digitalInputs: ["the_echo_pin"],
		digitalOutputs: ["the_ping_pin"]
	})
];


// State
export interface Experiment {
	filename: string;
	path: string;
	title: string;
	desc: string;
	device: Device;
	sensors: Sensor[];
}

export const defaultExperiment: Experiment = {
	filename: "",
	path: "",
	title: "",
	desc: "",
	device: Device.SensorBoardNanoRev1,
	sensors: []
};