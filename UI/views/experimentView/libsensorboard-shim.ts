import * as libsensorboard from "../../../Common/libsensorboard"
import {analogInputPins} from "./model";

export type PIN_TYPE = libsensorboard.PIN_TYPE;
export const PIN_TYPE = libsensorboard.PIN_TYPE;
export type PIN_MODE = libsensorboard.PIN_MODE;
export const PIN_MODE = libsensorboard.PIN_MODE;

export function pinMode(pinType: PIN_TYPE, pin: number, pinMode?: PIN_MODE) {
	pin = analogInputPins.indexOf(pin)-1;

	// noinspection JSIgnoredPromiseFromCall
	fetch(`/api/pinMode/${pinType}/${pin}/${pinMode}`);
}

export function analogPin(pin: number) {
	pinMode(PIN_TYPE.ANALOG,
			pin,
			PIN_MODE.ON);
}

export async function analogRead(pin: number): Promise<number> {
	pin = analogInputPins.indexOf(pin) - 1;
	let response = await fetch(`/api/analogRead/${pin}`);
	let text = await response.text();

	return parseInt(text) / 1024 * 5;
}

export function reset(): void {
	// noinspection JSIgnoredPromiseFromCall
	fetch(`/api/reset`);
}