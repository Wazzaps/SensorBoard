import {analogPin, analogRead, PIN_MODE, PIN_TYPE, pinMode} from "libsensorboard-shim";
import {Sensor} from "../model";

export function init(sensor: Sensor): void {
	analogPin(sensor.analogInputs[0]);
}

export async function value(sensor: Sensor): Promise<number> {
	if (sensor.analogInputs[0] == 0) {
		return -1;
	}
	return await analogRead(sensor.analogInputs[0]);
}

export async function prettyValue(sensor: Sensor): Promise<string> {
	return `${Math.round(await value(sensor) * 100) / 100} Volts`;
}