import {$1} from "../../misc/utils";

rivets.components["hamburger-menu"] = {
	template: () => $1("#hamburger-template").innerHTML,
	initialize: (el, attribs) => ({
		items: attribs.menuItemsAttr,
		closeMenu: (evt, scope) => attribs.menuOpenAttr = false
	})
};