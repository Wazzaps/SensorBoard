"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("../../misc/utils");
rivets.components["hamburger-menu"] = {
    template: function () { return utils_1.$1("#hamburger-template").innerHTML; },
    initialize: function (el, attribs) { return ({
        items: attribs.menuItemsAttr,
        closeMenu: function (evt, scope) { return attribs.menuOpenAttr = false; }
    }); }
};
