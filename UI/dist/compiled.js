var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
System.register("UI/misc/fixes", ["UI/misc/utils"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function default_1() {
        // Nav button blurring on click
        for (let btn of utils_1.$$(".unfocus")) {
            btn.addEventListener("mouseup", () => btn.blur());
        }
    }
    exports_1("default", default_1);
    var utils_1;
    return {
        setters: [
            function (utils_1_1) {
                utils_1 = utils_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("UI/views/welcomeView/model", [], function (exports_2, context_2) {
    "use strict";
    var __moduleName = context_2 && context_2.id;
    var recents;
    return {
        setters: [],
        execute: function () {
            exports_2("recents", recents = [
                {
                    "title": "Testing 1",
                    "path": "",
                    "type": "experiment"
                },
                {
                    "title": "Testing 2",
                    "path": "/home/david/SensorBoard/Experiments/test2.sbe",
                    "type": "experiment"
                },
                {
                    "title": "Testing 3",
                    "path": "/home/david/SensorBoard/Experiments/test3.sbp",
                    "type": "program"
                },
                {
                    "title": "Testing 4",
                    "path": "/home/david/SensorBoard/Experiments/test4.sbp",
                    "type": "program"
                }
            ]);
        }
    };
});
System.register("UI/views/welcomeView/view", ["UI/views/welcomeView/model", "UI/l10n/l10n", "UI/misc/utils"], function (exports_3, context_3) {
    "use strict";
    var __moduleName = context_3 && context_3.id;
    function init(root) {
        return rivets.bind(root, {
            recents: model_1.recents,
            remove_recent: (mouse_evt, state) => {
                model_1.recents.splice(state.index, 1);
            },
            change_l10n: (mouse_evt) => {
                l10n.change_l10n(mouse_evt.target.getAttribute("data-to"));
            },
            newExperiment: () => {
                // TODO: Setup state
                page.show("/experiment_setup");
                utils_2.$1("#experiment-title-input").focus();
            }
        });
    }
    exports_3("init", init);
    function focus(ctx) {
    }
    exports_3("focus", focus);
    var model_1, l10n, utils_2;
    return {
        setters: [
            function (model_1_1) {
                model_1 = model_1_1;
            },
            function (l10n_1) {
                l10n = l10n_1;
            },
            function (utils_2_1) {
                utils_2 = utils_2_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("Common/libsensorboard", [], function (exports_4, context_4) {
    "use strict";
    var __moduleName = context_4 && context_4.id;
    // Meta stuff
    function setSender(func) {
        sender = func;
    }
    exports_4("setSender", setSender);
    function* packetReceiver() {
        while (true) {
            let cmd = yield;
            //console.log(cmd >> 5);
            if (cmd >> 5 == 0x04) {
                let pin = (cmd & 0x03) >> 2;
                let value = (cmd & 0x03) + (yield);
                console.log("Voltage @" + pin + " = " + value);
                analogValues[pin] = value;
            }
            else if (cmd >> 5 == 0x02) {
            }
        }
    }
    function ultraSonicBegin(pingPin, echoPin, rate) {
        rate = rate || 1;
        if (pingPin < 8 && echoPin < 8) {
            sender([0x10, (pingPin << 4) + echoPin, rate - 1]); // 00 01 00 00, pp pp ee ee, rr rr rr rr
        }
        else {
            sender([0x11, pingPin, echoPin, rate - 1]); // 00 01 00 01, pp pp pp pp, ee ee ee ee, rr rr rr rr
        }
    }
    exports_4("ultraSonicBegin", ultraSonicBegin);
    function pinMode(pinType, pin, pinMode) {
        pinMode = pinMode === undefined ? PIN_MODE.ON : pinMode;
        if (pin < 8 && pinMode <= 1) {
            sender([0xC0 + (pinType << 4) + (pin << 1) + pinMode]); // 11 0t pp pm
        }
        else {
            sender([0xE0 + (pinType << 4) + (pin >> 6), ((pin & 0xFC) << 2) + pinMode]); // 11 1t pp pp, pp pp pp mm
        }
    }
    exports_4("pinMode", pinMode);
    function digitalWrite(pin, value) {
        if (pin < 16) {
            sender([(0x40) + (pin << 1) + (value & 0x01)]); // 01 0p pp pv
        }
        else {
            // TODO
            sender([(0x60) + (pin >> 6), (pin << 2) + (value & 0x01)]); // 01 10 pp pp, pp pp pp 0v
        }
    }
    exports_4("digitalWrite", digitalWrite);
    function reset() {
        sender([0x70]);
    }
    exports_4("reset", reset);
    //////////////////
    function test() {
        //reset();
        /*setInterval(() => {
            digitalWrite(0, 1);
            setTimeout(() => {
                digitalWrite(0, 0);
            }, 500);
        }, 1000);*/
        pinMode(PIN_TYPE.ANALOG, 5, PIN_MODE.ON);
        //ultraSonicBegin(0, 1, 10);
    }
    exports_4("test", test);
    var sender, customHandlers, analogValues, receiver, PIN_TYPE, PIN_MODE, ainValues;
    return {
        setters: [],
        execute: function () {
            exports_4("analogValues", analogValues = []);
            exports_4("receiver", receiver = packetReceiver());
            receiver.next();
            // Commands
            (function (PIN_TYPE) {
                PIN_TYPE[PIN_TYPE["DIGITAL"] = 0] = "DIGITAL";
                PIN_TYPE[PIN_TYPE["ANALOG"] = 1] = "ANALOG";
            })(PIN_TYPE || (PIN_TYPE = {}));
            exports_4("PIN_TYPE", PIN_TYPE);
            (function (PIN_MODE) {
                PIN_MODE[PIN_MODE["NONE"] = 2] = "NONE";
                PIN_MODE[PIN_MODE["INPUT"] = 3] = "INPUT";
                PIN_MODE[PIN_MODE["INPUT_PULLUP"] = 0] = "INPUT_PULLUP";
                PIN_MODE[PIN_MODE["OUTPUT"] = 1] = "OUTPUT";
                PIN_MODE[PIN_MODE["OFF"] = 0] = "OFF";
                PIN_MODE[PIN_MODE["ON"] = 1] = "ON";
            })(PIN_MODE || (PIN_MODE = {}));
            exports_4("PIN_MODE", PIN_MODE);
            // Variables
            ainValues = [];
            ainValues.length;
        }
    };
});
System.register("UI/views/experimentView/libsensorboard-shim", ["Common/libsensorboard", "UI/views/experimentView/model"], function (exports_5, context_5) {
    "use strict";
    var __moduleName = context_5 && context_5.id;
    function pinMode(pinType, pin, pinMode) {
        pin = model_2.analogInputPins.indexOf(pin) - 1;
        // noinspection JSIgnoredPromiseFromCall
        fetch(`/api/pinMode/${pinType}/${pin}/${pinMode}`);
    }
    exports_5("pinMode", pinMode);
    function analogPin(pin) {
        pinMode(PIN_TYPE.ANALOG, pin, PIN_MODE.ON);
    }
    exports_5("analogPin", analogPin);
    function analogRead(pin) {
        return __awaiter(this, void 0, void 0, function* () {
            pin = model_2.analogInputPins.indexOf(pin) - 1;
            let response = yield fetch(`/api/analogRead/${pin}`);
            let text = yield response.text();
            return parseInt(text) / 1024 * 5;
        });
    }
    exports_5("analogRead", analogRead);
    function reset() {
        // noinspection JSIgnoredPromiseFromCall
        fetch(`/api/reset`);
    }
    exports_5("reset", reset);
    var libsensorboard, model_2, PIN_TYPE, PIN_MODE;
    return {
        setters: [
            function (libsensorboard_1) {
                libsensorboard = libsensorboard_1;
            },
            function (model_2_1) {
                model_2 = model_2_1;
            }
        ],
        execute: function () {
            exports_5("PIN_TYPE", PIN_TYPE = libsensorboard.PIN_TYPE);
            exports_5("PIN_MODE", PIN_MODE = libsensorboard.PIN_MODE);
        }
    };
});
System.register("UI/views/experimentView/sensors/AnalogInput", ["UI/views/experimentView/libsensorboard-shim"], function (exports_6, context_6) {
    "use strict";
    var __moduleName = context_6 && context_6.id;
    function init(sensor) {
        libsensorboard_shim_1.analogPin(sensor.analogInputs[0]);
    }
    exports_6("init", init);
    function value(sensor) {
        return __awaiter(this, void 0, void 0, function* () {
            if (sensor.analogInputs[0] == 0) {
                return -1;
            }
            return yield libsensorboard_shim_1.analogRead(sensor.analogInputs[0]);
        });
    }
    exports_6("value", value);
    function prettyValue(sensor) {
        return __awaiter(this, void 0, void 0, function* () {
            return `${Math.round((yield value(sensor)) * 100) / 100} Volts`;
        });
    }
    exports_6("prettyValue", prettyValue);
    var libsensorboard_shim_1;
    return {
        setters: [
            function (libsensorboard_shim_1_1) {
                libsensorboard_shim_1 = libsensorboard_shim_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("UI/views/experimentView/model", ["UI/views/experimentView/sensors/AnalogInput"], function (exports_7, context_7) {
    "use strict";
    var __moduleName = context_7 && context_7.id;
    var AnalogInput, Device, deviceNames, defaultSensorType, defaultSensor, analogInputPins, analogOutputPins, digitalInputPins, digitalOutputPins, sensorTypes, defaultExperiment;
    return {
        setters: [
            function (AnalogInput_1) {
                AnalogInput = AnalogInput_1;
            }
        ],
        execute: function () {
            (function (Device) {
                Device[Device["SensorBoardNanoRev1"] = 0] = "SensorBoardNanoRev1";
            })(Device || (Device = {}));
            exports_7("Device", Device);
            exports_7("deviceNames", deviceNames = [
                "SensorBoard Nano Rev1"
            ]);
            exports_7("defaultSensorType", defaultSensorType = {
                "name": "Unnamed",
                "desc": "",
                "image": "",
                analogInputs: [],
                analogOutputs: [],
                digitalInputs: [],
                digitalOutputs: [],
                I2CAddress: "",
                configurableI2CAddress: false,
                hasSPIConnector: false,
                hasUARTConnector: false,
                initFunc: () => console.log("UNDEFINED SENSOR INIT"),
                valueFunc: () => -1,
                prettyValueFunc: () => "UNDEFINED"
            });
            exports_7("defaultSensor", defaultSensor = {
                type: 0,
                analogInputs: [],
                analogOutputs: [],
                digitalInputs: [],
                digitalOutputs: [],
                I2CAddress: -1,
                SPIConnector: -1,
                UARTConnector: -1,
                visOpen: false,
                visValue: 0,
                visPrettyValue: ""
            });
            exports_7("analogInputPins", analogInputPins = [0, 8, 9, 10, 11, 12, 13]);
            exports_7("analogOutputPins", analogOutputPins = [0, 2, 4, 5]);
            exports_7("digitalInputPins", digitalInputPins = [0, 1, 2, 3, 4, 5, 6, 7]);
            exports_7("digitalOutputPins", digitalOutputPins = [0, 1, 2, 3, 4, 5, 6, 7]);
            exports_7("sensorTypes", sensorTypes = [
                Object.assign({}, defaultSensorType, {
                    "name": "digital_input",
                    "desc": "Lorem ipsum",
                    digitalInputs: ["the_input_pin"]
                }),
                Object.assign({}, defaultSensorType, {
                    "name": "analog_input",
                    "desc": "Lorem ipsum",
                    analogInputs: ["the_input_pin"],
                    initFunc: AnalogInput.init,
                    valueFunc: AnalogInput.value,
                    prettyValueFunc: AnalogInput.prettyValue
                }),
                Object.assign({}, defaultSensorType, {
                    "name": "ultrasonic_distance_sensor",
                    "desc": "Lorem ipsum",
                    digitalInputs: ["the_echo_pin"],
                    digitalOutputs: ["the_ping_pin"]
                })
            ]);
            exports_7("defaultExperiment", defaultExperiment = {
                filename: "",
                path: "",
                title: "",
                desc: "",
                device: Device.SensorBoardNanoRev1,
                sensors: []
            });
        }
    };
});
System.register("UI/controllers/dataVis", ["UI/views/experimentView/sensors/AnalogInput", "UI/views/experimentView/libsensorboard-shim", "UI/controllers/main"], function (exports_8, context_8) {
    "use strict";
    var __moduleName = context_8 && context_8.id;
    function update() {
        console.log("Update!");
        libsensorboard_shim_2.reset();
        main_1.globalstate.experiment.currentExperiment.sensors.forEach(sensor => {
            console.log(sensor);
            if (sensor.type == 1) {
                AnalogInput.init(sensor);
            }
        });
    }
    exports_8("update", update);
    var AnalogInput, libsensorboard_shim_2, main_1;
    return {
        setters: [
            function (AnalogInput_2) {
                AnalogInput = AnalogInput_2;
            },
            function (libsensorboard_shim_2_1) {
                libsensorboard_shim_2 = libsensorboard_shim_2_1;
            },
            function (main_1_1) {
                main_1 = main_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("UI/views/experimentView/view", ["UI/views/experimentView/model", "UI/misc/utils", "UI/controllers/main", "UI/l10n/l10n", "UI/controllers/dataVis", "UI/views/experimentView/sensors/AnalogInput"], function (exports_9, context_9) {
    "use strict";
    var __moduleName = context_9 && context_9.id;
    function init(root) {
        rivets.formatters.getSensorType = sensor_id => model_3.sensorTypes[sensor_id];
        expState = main_2.globalstate.experiment;
        root.addEventListener("click", (evt) => {
            if (!evt.target.classList.contains("hamburger-menu-item") && evt.target.id !== "experiment-menu-btn") {
                expState.state.hamMenuOpen = false;
            }
        });
        setInterval(() => {
            expState.currentExperiment.sensors.forEach((sensor) => __awaiter(this, void 0, void 0, function* () {
                if (sensor.type == 1) {
                    console.log(yield AnalogInput.prettyValue(sensor));
                }
            }));
        }, 2000);
        return rivets.bind(root, {
            // State
            exp: expState.currentExperiment,
            state: expState.state,
            hamMenuItems: [
                {
                    name: "open",
                    onclick: () => {
                    }
                },
                {
                    name: "save",
                    onclick: () => {
                    }
                },
                {
                    name: "change_lang",
                    onclick: () => {
                        if (l10n_2._l10n.current_lang == "en_us") {
                            l10n_2.change_l10n("he_il");
                        }
                        else {
                            l10n_2.change_l10n("en_us");
                        }
                    }
                },
            ],
            // Static data
            deviceSelection: model_3.deviceNames,
            analogInPossiblePins: model_3.analogInputPins,
            analogOutPossiblePins: model_3.analogOutputPins,
            digitalInPossiblePins: model_3.digitalInputPins,
            digitalOutPossiblePins: model_3.digitalOutputPins,
            sensorTypes: model_3.sensorTypes,
            // Methods
            toggleExperimentDetailsView: () => {
                expState.state.detailsOpen = !expState.state.detailsOpen;
                if (expState.state.detailsOpen) {
                    utils_3.$1("#experiment-title-input").focus();
                    if (expState.currentExperiment.title == "Empty Title") {
                        expState.currentExperiment.title = "";
                    }
                }
                else if (expState.currentExperiment.title == "") {
                    expState.currentExperiment.title = "Empty Title";
                }
            },
            toggleMenu: () => {
                /*if (_l10n.current_lang == "en_us") {
                    change_l10n("he_il");
                } else {
                    change_l10n("en_us");
                }*/
                expState.state.hamMenuOpen = !expState.state.hamMenuOpen;
            },
            remove_sensor: (mouse_evt, state) => {
                expState.currentExperiment.sensors.splice(state.index, 1);
            },
            add_sensor: () => {
                expState.currentExperiment.sensors.push(Object.assign({}, model_3.defaultSensor, {
                    type: expState.state.newSensorSelection,
                    analogInputs: [0],
                    analogOutputs: new Array(model_3.sensorTypes[expState.state.newSensorSelection].analogOutputs.length).fill(0),
                    digitalInputs: new Array(model_3.sensorTypes[expState.state.newSensorSelection].digitalInputs.length).fill(0),
                    digitalOutputs: new Array(model_3.sensorTypes[expState.state.newSensorSelection].digitalOutputs.length).fill(0)
                }));
            },
            updated: (evt, state) => {
                //state["%sensor%"] // Sensor number
                //state["%ain%/%aout%/%din%/%dout%"] // Pin number in sensor
                dataVis.update();
            },
            s: main_2.globalstate
        });
    }
    exports_9("init", init);
    function focus(ctx) {
    }
    exports_9("focus", focus);
    var model_3, utils_3, main_2, l10n_2, dataVis, AnalogInput, expState;
    return {
        setters: [
            function (model_3_1) {
                model_3 = model_3_1;
            },
            function (utils_3_1) {
                utils_3 = utils_3_1;
            },
            function (main_2_1) {
                main_2 = main_2_1;
            },
            function (l10n_2_1) {
                l10n_2 = l10n_2_1;
            },
            function (dataVis_1) {
                dataVis = dataVis_1;
            },
            function (AnalogInput_3) {
                AnalogInput = AnalogInput_3;
            }
        ],
        execute: function () {
        }
    };
});
System.register("UI/controllers/main", ["UI/misc/utils", "UI/misc/fixes", "UI/views/welcomeView/view", "UI/views/experimentView/view", "UI/l10n/l10n", "UI/views/experimentView/model", "UI/components/hamburgerMenu/component"], function (exports_10, context_10) {
    "use strict";
    var __moduleName = context_10 && context_10.id;
    var utils_4, fixes, welcomeView, experimentView, l10n_3, model_4, globalstate, views, activeView;
    return {
        setters: [
            function (utils_4_1) {
                utils_4 = utils_4_1;
            },
            function (fixes_1) {
                fixes = fixes_1;
            },
            function (welcomeView_1) {
                welcomeView = welcomeView_1;
            },
            function (experimentView_1) {
                experimentView = experimentView_1;
            },
            function (l10n_3_1) {
                l10n_3 = l10n_3_1;
            },
            function (model_4_1) {
                model_4 = model_4_1;
            },
            function (_1) {
            }
        ],
        execute: function () {
            // Global state
            exports_10("globalstate", globalstate = {
                experiment: {
                    state: {
                        detailsOpen: true,
                        hamMenuOpen: false,
                        newSensorSelection: 0
                    },
                    currentExperiment: Object.assign({}, model_4.defaultExperiment)
                },
                theme: "white",
                wtf: ["0"]
            });
            window.__state = globalstate;
            // Setup Page.JS router
            exports_10("views", views = utils_4.zip([
                welcomeView,
                experimentView
            ], utils_4.$$(".view"), []));
            activeView = views[0];
            views.forEach(v => {
                v[2] = v[0].init(v[1]); // Save binding for later
                page(v[1].getAttribute("x-view-path"), ctx => {
                    //console.log("v[1].getAttribute(\"x-view-path\")");
                    activeView[1].style.display = "none";
                    activeView = v;
                    document.title = "SensorBoard Programmer - " + l10n_3.l10n(v[1].getAttribute("x-view-title"));
                    v[1].style.display = "grid";
                    v[0].focus(ctx);
                });
            });
            rivets.bind(utils_4.$1("#theme-link"), globalstate);
            page.start({
                hashbang: true
            });
            // Random fixes
            fixes.default();
        }
    };
});
System.register("UI/l10n/l10n", ["UI/controllers/main", "UI/misc/utils"], function (exports_11, context_11) {
    "use strict";
    var __moduleName = context_11 && context_11.id;
    function l10n(phrase) {
        return _l10n[_l10n.current_lang][phrase] || (`<${phrase}>`);
    }
    exports_11("l10n", l10n);
    function change_l10n(symbol) {
        main_3.views.forEach(v => v[2].unbind());
        _l10n.current_lang = symbol;
        utils_5.$1("body").setAttribute("dir", l10n("_dir"));
        main_3.views.forEach(v => v[2].bind());
        page.show(page.current);
    }
    exports_11("change_l10n", change_l10n);
    var main_3, utils_5, _l10n;
    return {
        setters: [
            function (main_3_1) {
                main_3 = main_3_1;
            },
            function (utils_5_1) {
                utils_5 = utils_5_1;
            }
        ],
        execute: function () {
            exports_11("_l10n", _l10n = {
                "current_lang": "en_us",
                "en_us": {
                    "_dir": "ltr",
                    // Global
                    "change_lang": "Change Language",
                    "open": "Open",
                    "save": "Save",
                    // WelcomeView
                    "welcome": "Welcome",
                    "new_experiment": "New Experiment",
                    "new_program": "New Program",
                    "no_recent_projects": "No recent projects",
                    // ExperimentView
                    "experiment_setup": "Experiment Setup",
                    "empty_title": "Empty Title",
                    "description": "Description",
                    "device_colon": "Device:",
                    "create_new_sensor": "Create new sensor",
                    // Sensors
                    "digital_input": "Digital Input",
                    "the_input_pin": "The input pin",
                    "analog_input": "Analog Input",
                    "ultrasonic_distance_sensor": "Ultrasonic distance sensor",
                    "the_echo_pin": "The echo pin",
                    "the_ping_pin": "The ping pin"
                },
                "he_il": {
                    "_dir": "rtl",
                    // Global
                    "change_lang": "שנה שפה",
                    "open": "פתח",
                    "save": "שמור",
                    // WelcomeView
                    "welcome": "ברוכים הבאים",
                    "new_experiment": "ניסוי חדש",
                    "new_program": "תוכנה חדשה",
                    "no_recent_projects": "אין פרוייקטים אחרונים",
                    // ExperimentView
                    "experiment_setup": "תכנון ניסוי",
                    "empty_title": "כותרת ריקה",
                    "description": "תיאור הניסוי",
                    "device_colon": "מכשיר:",
                    "create_new_sensor": "צור חיישן חדש",
                    // Sensors
                    "digital_input": "כניסה דיגיטלית",
                    "the_input_pin": "חיבור הקלט",
                    "analog_input": "כניסה אנלוגית",
                    "ultrasonic_distance_sensor": "חיישן מרחק אולטרסוני",
                    "the_echo_pin": "חיבור האקו",
                    "the_ping_pin": "חיבור הפינג"
                }
            });
        }
    };
});
System.register("UI/misc/utils", ["UI/l10n/l10n"], function (exports_12, context_12) {
    "use strict";
    var __moduleName = context_12 && context_12.id;
    var l10n_4, $$, $1, zip;
    return {
        setters: [
            function (l10n_4_1) {
                l10n_4 = l10n_4_1;
            }
        ],
        execute: function () {
            exports_12("$$", $$ = (query) => [...document.querySelectorAll(query)]);
            exports_12("$1", $1 = (query) => document.querySelector(query));
            exports_12("zip", zip = (...arrs) => arrs[0].map((a, i) => arrs.map(arr => arr[i])));
            rivets.formatters.length = value => value.length;
            rivets.formatters.ternary = (value, a, b) => value ? a : b;
            rivets.formatters.slice = (value, a, b) => value.slice(parseInt(a), parseInt(b));
            rivets.formatters.isEq = (value, a) => value == a;
            rivets.formatters.get = (value, i) => value[i];
            rivets.formatters.stringify = (value) => JSON.stringify(value);
            rivets.formatters["+"] = (a, b) => a + b;
            rivets.formatters.l10n = value => l10n_4.l10n(value);
            rivets.binders.l10n = (el, value) => {
                el.textContent = l10n_4.l10n(value);
            };
        }
    };
});
System.register("UI/components/hamburgerMenu/component", ["UI/misc/utils"], function (exports_13, context_13) {
    "use strict";
    var __moduleName = context_13 && context_13.id;
    var utils_6;
    return {
        setters: [
            function (utils_6_1) {
                utils_6 = utils_6_1;
            }
        ],
        execute: function () {
            rivets.components["hamburger-menu"] = {
                template: () => utils_6.$1("#hamburger-template").innerHTML,
                initialize: (el, attribs) => ({
                    items: attribs.menuItemsAttr,
                    closeMenu: (evt, scope) => attribs.menuOpenAttr = false
                })
            };
        }
    };
});
//# sourceMappingURL=compiled.js.map