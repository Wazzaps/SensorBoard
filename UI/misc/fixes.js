"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("./utils");
function default_1() {
    var _loop_1 = function (btn) {
        btn.addEventListener("mouseup", function () { return btn.blur(); });
    };
    // Nav button blurring on click
    for (var _i = 0, _a = utils_1.$$(".unfocus"); _i < _a.length; _i++) {
        var btn = _a[_i];
        _loop_1(btn);
    }
}
exports.default = default_1;
