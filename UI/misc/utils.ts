import {l10n} from "../l10n/l10n";

export let $$ = (query:string) : HTMLElement[] => [...document.querySelectorAll(query)] as HTMLElement[];
export let $1 = (query:string) : HTMLElement => document.querySelector(query) as HTMLElement;
export let zip = (...arrs: any[]) => arrs[0].map((a, i) => arrs.map(arr => arr[i]));

rivets.formatters.length = value => value.length;
rivets.formatters.ternary = (value, a, b) => value ? a : b;
rivets.formatters.slice = (value: any[], a, b) => value.slice(parseInt(a), parseInt(b));
rivets.formatters.isEq = (value, a) => value == a;
rivets.formatters.get = (value, i) => value[i];
rivets.formatters.stringify = (value) => JSON.stringify(value);
rivets.formatters["+"] = (a, b) => a + b;
rivets.formatters.l10n = value => l10n(value);
rivets.binders.l10n = (el: HTMLElement, value: string) => {
	el.textContent = l10n(value);
};