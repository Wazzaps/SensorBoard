"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var l10n_1 = require("../l10n/l10n");
exports.$$ = function (query) { return document.querySelectorAll(query).slice(); };
exports.$1 = function (query) { return document.querySelector(query); };
exports.zip = function () {
    var arrs = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        arrs[_i] = arguments[_i];
    }
    return arrs[0].map(function (a, i) { return arrs.map(function (arr) { return arr[i]; }); });
};
rivets.formatters.length = function (value) { return value.length; };
rivets.formatters.ternary = function (value, a, b) { return value ? a : b; };
rivets.formatters.isEq = function (value, a) { return value == a; };
rivets.formatters.get = function (value, i) { return value[i]; };
rivets.formatters["+"] = function (a, b) { return a + b; };
rivets.formatters.l10n = function (value) { return l10n_1.l10n(value); };
rivets.binders.l10n = function (el, value) {
    el.textContent = l10n_1.l10n(value);
};
