import {$$} from "./utils";

export default function() {
	// Nav button blurring on click
	for (let btn of $$(".unfocus")) {
		btn.addEventListener("mouseup", () => btn.blur());
	}
}
