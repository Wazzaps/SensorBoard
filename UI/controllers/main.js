"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("../misc/utils");
var fixes = require("../misc/fixes");
var welcomeView = require("../views/welcomeView/view");
var experimentView = require("../views/experimentView/view");
var l10n_1 = require("../l10n/l10n");
var model_1 = require("../views/experimentView/model");
require("../components/hamburger_menu/component");
// Global state
exports.globalstate = {
    experiment: {
        state: {
            detailsOpen: true,
            newSensorSelection: 0
        },
        currentExperiment: Object.assign({}, model_1.defaultExperiment)
    },
    theme: "white"
};
window.__state = exports.globalstate;
// Setup Page.JS router
exports.views = utils_1.zip([
    welcomeView,
    experimentView
], utils_1.$$(".view"), []);
var activeView = exports.views[0];
exports.views.forEach(function (v) {
    v[2] = v[0].init(v[1]); // Save binding for later
    page(v[1].getAttribute("x-view-path"), function (ctx) {
        //console.log("v[1].getAttribute(\"x-view-path\")");
        activeView[1].style.display = "none";
        activeView = v;
        document.title = "SensorBoard Programmer - " + l10n_1.l10n(v[1].getAttribute("x-view-title"));
        v[1].style.display = "grid";
        v[0].focus(ctx);
    });
});
rivets.bind(utils_1.$1("#theme-link"), exports.globalstate);
page.start({
    hashbang: true
});
// Random fixes
fixes.default();
