import {$$, $1, zip} from "../misc/utils";
import * as fixes from "../misc/fixes";

import * as welcomeView from "../views/welcomeView/view";
import * as experimentView from "../views/experimentView/view";
import {l10n} from "../l10n/l10n";
import {defaultExperiment} from "../views/experimentView/model";

import "../components/hamburgerMenu/component";

// Global state
export let globalstate = {
	experiment: {
		state: {
			detailsOpen: true,
			hamMenuOpen: false,
			newSensorSelection: 0
		},
		currentExperiment: Object.assign({}, defaultExperiment)
	},
	theme: "white",
	wtf: ["0"]
};

(window as any).__state = globalstate;

// Setup Page.JS router
export let views = zip([
	welcomeView,
	experimentView
], $$(".view"), []);
let activeView = views[0];

views.forEach(v => {
	v[2] = v[0].init(v[1]); // Save binding for later

	page(v[1].getAttribute("x-view-path"), ctx => {
		//console.log("v[1].getAttribute(\"x-view-path\")");
		activeView[1].style.display = "none";
		activeView = v;
		document.title = "SensorBoard Programmer - " + l10n(v[1].getAttribute("x-view-title"));
		v[1].style.display = "grid";
		v[0].focus(ctx);
	});
});

rivets.bind($1("#theme-link"), globalstate);

page.start({
	hashbang: true
});

// Random fixes
fixes.default();