import * as AnalogInput from "../views/experimentView/sensors/AnalogInput";
import {reset} from "../views/experimentView/libsensorboard-shim";
import {globalstate} from "./main";

export function update() {
	console.log("Update!");
	reset();
	globalstate.experiment.currentExperiment.sensors.forEach(sensor => {
		console.log(sensor);
		if (sensor.type == 1) { // Analog Input
			AnalogInput.init(sensor);
		}
	});

	
}