import {views} from "../controllers/main";
import {$1} from "../misc/utils";

export let _l10n = {
	"current_lang": "en_us",
	"en_us": {
		"_dir": "ltr",

		// Global
		"change_lang": "Change Language",
		"open": "Open",
		"save": "Save",

		// WelcomeView
		"welcome": "Welcome",
		"new_experiment": "New Experiment",
		"new_program": "New Program",
		"no_recent_projects": "No recent projects",

		// ExperimentView
		"experiment_setup": "Experiment Setup",
		"empty_title": "Empty Title",
		"description": "Description",
		"device_colon": "Device:",
		"create_new_sensor": "Create new sensor",

		// Sensors
		"digital_input": "Digital Input",
		"the_input_pin": "The input pin",

		"analog_input": "Analog Input",

		"ultrasonic_distance_sensor": "Ultrasonic distance sensor",
		"the_echo_pin": "The echo pin",
		"the_ping_pin": "The ping pin"
	},
	"he_il": {
		"_dir": "rtl",

		// Global
		"change_lang": "שנה שפה",
		"open": "פתח",
		"save": "שמור",

		// WelcomeView
		"welcome": "ברוכים הבאים",
		"new_experiment": "ניסוי חדש",
		"new_program": "תוכנה חדשה",
		"no_recent_projects": "אין פרוייקטים אחרונים",

		// ExperimentView
		"experiment_setup": "תכנון ניסוי",
		"empty_title": "כותרת ריקה",
		"description": "תיאור הניסוי",
		"device_colon": "מכשיר:",
		"create_new_sensor": "צור חיישן חדש",

		// Sensors
		"digital_input": "כניסה דיגיטלית",
		"the_input_pin": "חיבור הקלט",

		"analog_input": "כניסה אנלוגית",

		"ultrasonic_distance_sensor": "חיישן מרחק אולטרסוני",
		"the_echo_pin": "חיבור האקו",
		"the_ping_pin": "חיבור הפינג"
	}
};

export function l10n(phrase: string): string {
	return _l10n[_l10n.current_lang][phrase] || (`<${phrase}>`);
}

export function change_l10n(symbol: string) {
	views.forEach(v => v[2].unbind());
	_l10n.current_lang = symbol;

	$1("body").setAttribute("dir", l10n("_dir"));
	views.forEach(v => v[2].bind());
	page.show(page.current);
}