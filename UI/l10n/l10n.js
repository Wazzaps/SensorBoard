"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var main_1 = require("../controllers/main");
var utils_1 = require("../misc/utils");
exports._l10n = {
    "current_lang": "en_us",
    "en_us": {
        "_dir": "ltr",
        // WelcomeView
        "welcome": "Welcome",
        "new_experiment": "New Experiment",
        "new_program": "New Program",
        "open": "Open",
        "no_recent_projects": "No recent projects",
        // ExperimentView
        "experiment_setup": "Experiment Setup",
        "empty_title": "Empty Title",
        "description": "Description",
        "device_colon": "Device:",
        "create_new_sensor": "Create new sensor",
        // Sensors
        "digital_input": "Digital Input",
        "the_input_pin": "The input pin",
        "analog_input": "Analog Input",
        "ultrasonic_distance_sensor": "Ultrasonic distance sensor",
        "the_echo_pin": "The echo pin",
        "the_ping_pin": "The ping pin"
    },
    "he_il": {
        "_dir": "rtl",
        // WelcomeView
        "welcome": "ברוכים הבאים",
        "new_experiment": "ניסוי חדש",
        "new_program": "תוכנה חדשה",
        "open": "פתח",
        "no_recent_projects": "אין פרוייקטים אחרונים",
        // ExperimentView
        "experiment_setup": "תכנון ניסוי",
        "empty_title": "כותרת ריקה",
        "description": "תיאור הניסוי",
        "device_colon": "מכשיר:",
        "create_new_sensor": "צור חיישן חדש",
        // Sensors
        "digital_input": "כניסה דיגיטלית",
        "the_input_pin": "חיבור הקלט",
        "analog_input": "כניסה אנלוגית",
        "ultrasonic_distance_sensor": "חיישן מרחק אולטרסוני",
        "the_echo_pin": "חיבור האקו",
        "the_ping_pin": "חיבור הפינג"
    }
};
function l10n(phrase) {
    return exports._l10n[exports._l10n.current_lang][phrase];
}
exports.l10n = l10n;
function change_l10n(symbol) {
    main_1.views.forEach(function (v) { return v[2].unbind(); });
    exports._l10n.current_lang = symbol;
    utils_1.$1("body").setAttribute("dir", l10n("_dir"));
    main_1.views.forEach(function (v) { return v[2].bind(); });
}
exports.change_l10n = change_l10n;
