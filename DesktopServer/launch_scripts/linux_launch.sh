#!/usr/bin/env bash

if [ ! -e "/dev/rfcomm0" ]; then
    echo "File not found!"
    gksu -m "Enter root password for Bluetooth initialization" -- '/usr/bin/env bash' -c \
    	'rfcomm bind 0 all; sleep 1; \
    	 chmod 777 /dev/rfcomm0'
elif [ ! -x "/dev/rfcomm0" ]; then
    echo "Permission error"
    gksu -m "Enter root password for Bluetooth initialization" -- '/usr/bin/env bash' -c \
    	'chmod 777 /dev/rfcomm0'
else
	echo "File found!"
fi