#!/usr/bin/env bash

#gksu -m "Please enter your root password to complete setup"
#adduser `whoami` dialout
echo "# SENSORBOARD START" >> /etc/bluetooth/rfcomm.conf
echo "rfcomm0 {" >> /etc/bluetooth/rfcomm.conf
echo "  bind yes;" >> /etc/bluetooth/rfcomm.conf
echo "  device 20:16:11:17:17:80;" >> /etc/bluetooth/rfcomm.conf
echo "  channel 1;" >> /etc/bluetooth/rfcomm.conf
echo "  comment \"SensorBoard\";" >> /etc/bluetooth/rfcomm.conf
echo "}" >> /etc/bluetooth/rfcomm.conf
echo "# SENSORBOARD END" >> /etc/bluetooth/rfcomm.conf