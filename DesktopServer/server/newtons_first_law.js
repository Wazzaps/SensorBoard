"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function restifyArrayOfObjects(app, path, arrayFactory) {
    app.get(path + '/get_field/:field', function (req, res) {
        res.contentType("json");
        res.end(JSON.stringify(arrayFactory().map(function (dev) { return dev[req.params.field]; })));
    });
    app.get(path, function (req, res) {
        res.contentType("json");
        res.end(JSON.stringify(arrayFactory()));
    });
}
exports.restifyArrayOfObjects = restifyArrayOfObjects;
