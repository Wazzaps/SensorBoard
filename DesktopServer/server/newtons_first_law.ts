import core = require('express-serve-static-core');

export function restifyArrayOfObjects(app: core.Express, path: string, arrayFactory:() => any[]): void {
	app.get(path + '/get_field/:field', (req, res) => {
		res.contentType("json");
		res.end(
			JSON.stringify(arrayFactory().map(dev => dev[req.params.field]))
		);
	});

	app.get(path, (req, res) => {
		res.contentType("json");
		res.end(
			JSON.stringify(arrayFactory())
		);
	});
}
