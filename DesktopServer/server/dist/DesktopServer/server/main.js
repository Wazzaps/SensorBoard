"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const devicemanager_1 = require("./devicemanager");
const newtons_first_law_1 = require("./newtons_first_law");
const SensorBoard = require("../../Common/libsensorboard");
const app = express();
app.use(express.static("../UI/dist"));
newtons_first_law_1.restifyArrayOfObjects(app, '/api/devices', devicemanager_1.findBluetoothDevices);
// ultraSonicBegin
app.get("/api/digitalWrite/:pin/:to", (req, res) => {
    SensorBoard.digitalWrite(parseInt(req.params.pin), parseInt(req.params.to));
    res.end("OK");
});
app.get("/api/analogRead/:pin", (req, res) => {
    res.end(SensorBoard.analogValues[parseInt(req.params.pin)].toString());
});
app.get("/api/pinMode/:type/:pin/:mode", (req, res) => {
    SensorBoard.pinMode(parseInt(req.params.type), parseInt(req.params.type), parseInt(req.params.mode));
    res.end("OK");
});
app.get("/api/reset", (req, res) => {
    SensorBoard.reset();
    res.end("OK");
});
app.get("/api/__test", (req, res) => {
    console.log("Connecting to", "20:16:11:17:17:80");
    devicemanager_1.connectToBluetoothDevice("20:16:11:17:17:80", connection => {
        SensorBoard.setSender((data) => {
            connection.write(Buffer.from(data), () => { });
        });
        connection.on("data", (buffer) => {
            for (let i = 0; i < buffer.length; i++) {
                console.log(buffer[i]);
                SensorBoard.receiver.next(buffer[i]);
            }
        });
        SensorBoard.test();
    });
    res.end("OK");
});
app.get("/api/connect/:bt_id", (req, res) => {
    console.log("Connecting to", req.params.bt_id);
    devicemanager_1.connectToBluetoothDevice(req.params.bt_id, connection => {
        SensorBoard.setSender((data) => {
            connection.write(Buffer.from(data), () => { });
        });
        connection.on("data", (buffer) => {
            for (let i = 0; i < buffer.length; i++) {
                //console.log(buffer[i]);
                SensorBoard.receiver.next(buffer[i]);
            }
        });
        SensorBoard.test();
    });
    res.end("OK");
});
app.get("/api/disconnect", (req, res) => {
    console.log("Disconnecting from device");
    devicemanager_1.disconnectFromBluetoothDevice();
    res.end("OK");
});
app.listen(19123, "localhost", () => console.log('SensorBoard server listening on port 19123'));
//# sourceMappingURL=main.js.map