"use strict";
//const btSerial = new (require('bluetooth-serial-port')).BluetoothSerialPort();
Object.defineProperty(exports, "__esModule", { value: true });
const bluetooth = require('node-bluetooth');
//const repl = require('repl');
const child_process = require("child_process");
let btConnection;
//let firmataBoard;
let isBTConnected = false;
function findBluetoothDevices() {
    let output = child_process.execSync("bt-device -l", { timeout: 100, encoding: "utf8" })
        .split("\n");
    output.shift();
    output.pop();
    output = output.map(dev => dev.split(" ("));
    output = output.map((dev) => ({
        name: dev[0],
        bt_id: dev[1].slice(0, -1)
    }));
    return output;
}
exports.findBluetoothDevices = findBluetoothDevices;
function getBoard() {
    return isBTConnected ? btConnection : null;
}
exports.getBoard = getBoard;
function connectToBluetoothDevice(bt_id, callback) {
    bluetooth.connect(bt_id, 1, function (err, connection) {
        if (err)
            return console.error(err);
        isBTConnected = true;
        btConnection = connection;
        callback(connection);
        console.log("Connected to Bluetooth device");
    });
}
exports.connectToBluetoothDevice = connectToBluetoothDevice;
function disconnectFromBluetoothDevice() {
    btConnection.close();
}
exports.disconnectFromBluetoothDevice = disconnectFromBluetoothDevice;
//# sourceMappingURL=devicemanager.js.map