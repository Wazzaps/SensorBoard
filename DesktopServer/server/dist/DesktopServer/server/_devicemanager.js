"use strict";
//const btSerial = new (require('bluetooth-serial-port')).BluetoothSerialPort();
Object.defineProperty(exports, "__esModule", { value: true });
const bluetooth = require('node-bluetooth');
const firmata = require('firmata');
const repl = require('repl');
const child_process = require("child_process");
let btConnection;
let firmataBoard;
let isBTConnected = false;
let isFirmataConnected = false;
let devicesFound = [];
setInterval(() => {
    let output = child_process.execSync("bt-device -l", { timeout: 100, encoding: "utf8" })
        .split("\n");
    output.shift();
    output.pop();
    output = output.map(dev => dev.split(" ("));
    output = output.map((dev) => ({
        name: dev[0],
        bt_id: dev[1].slice(0, -1)
    }));
    devicesFound = output;
}, 1000);
// close the connection when you're ready
/*btSerial.on('found', (address, name) => {
    console.log(name, address);
    btSerial.findSerialPortChannel(address, channel => {
        console.log(name, address, channel);
        if (finishedScan) {
            devicesFound = [];
            finishedScan = false;
        }
        devicesFound.push({
            name,
            bt_id: address,
            channel: channel
        });
        connectToBluetoothDevice(address);
    }, function() {
        console.log('found nothing');
    });
});

btSerial.on("finished", () => {
    console.log('finished');
    finishedScan = true;
});

btSerial.inquire();*/
//setInterval(() => btSerial.inquire(), 10000);
function findBluetoothDevices() {
    return devicesFound;
}
exports.findBluetoothDevices = findBluetoothDevices;
function getBoard() {
    return isFirmataConnected ? firmataBoard : null;
}
exports.getBoard = getBoard;
function connectToBluetoothDevice(bt_id) {
    bluetooth.connect(bt_id, 1, function (err, connection) {
        if (err)
            return console.error(err);
        isBTConnected = true;
        btConnection = connection;
        firmataBoard = new firmata(connection, () => {
            isFirmataConnected = true;
            firmataBoard.pins[13].mode = 1;
            console.log("Connected to Firmata Service");
            repl.start("> ").context.brd = firmataBoard;
        });
        console.log("Connected to Bluetooth device");
    });
}
exports.connectToBluetoothDevice = connectToBluetoothDevice;
function cmdSetTime(hr, min, sec, day, month, yr) {
    firmataBoard.sysexCommand([0x01, hr, min, sec, day, month, yr].map(v => v & 0x7F));
}
exports.cmdSetTime = cmdSetTime;
function cmdUploadExperiment() {
    firmataBoard.sysexCommand([0x02]);
}
exports.cmdUploadExperiment = cmdUploadExperiment;
function cmdExecExperiment() {
    firmataBoard.sysexCommand([0x03]);
}
exports.cmdExecExperiment = cmdExecExperiment;
function cmdListLogs() {
    firmataBoard.sysexCommand([0x04]);
}
exports.cmdListLogs = cmdListLogs;
function cmdDownloadLog() {
    firmataBoard.sysexCommand([0x05]);
}
exports.cmdDownloadLog = cmdDownloadLog;
function disconnectFromBluetoothDevice() {
    btConnection.close();
}
exports.disconnectFromBluetoothDevice = disconnectFromBluetoothDevice;
//# sourceMappingURL=_devicemanager.js.map