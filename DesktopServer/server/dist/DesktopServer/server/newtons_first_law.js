"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function restifyArrayOfObjects(app, path, arrayFactory) {
    app.get(path + '/get_field/:field', (req, res) => {
        res.contentType("json");
        res.end(JSON.stringify(arrayFactory().map(dev => dev[req.params.field])));
    });
    app.get(path, (req, res) => {
        res.contentType("json");
        res.end(JSON.stringify(arrayFactory()));
    });
}
exports.restifyArrayOfObjects = restifyArrayOfObjects;
//# sourceMappingURL=newtons_first_law.js.map