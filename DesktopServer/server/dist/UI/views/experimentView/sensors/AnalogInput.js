"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const libsensorboard_shim_1 = require("libsensorboard-shim");
function init(sensor) {
    libsensorboard_shim_1.pinMode(libsensorboard_shim_1.PIN_TYPE.ANALOG, sensor.analogInputs[0], libsensorboard_shim_1.PIN_MODE.ON); // TODO: Map this
}
exports.init = init;
function value(sensor) {
    if (sensor.analogInputs[0] == 0) {
        return 0;
    }
    return libsensorboard_shim_1.analogRead(sensor.analogInputs[0]) / 1024 * 5;
}
exports.value = value;
function prettyValue(sensor) {
    return `${Math.round(value(sensor) * 100) / 100} Volts`;
}
exports.prettyValue = prettyValue;
//# sourceMappingURL=AnalogInput.js.map