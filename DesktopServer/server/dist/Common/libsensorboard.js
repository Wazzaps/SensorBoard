"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let sender;
exports.analogValues = [];
// Meta stuff
function setSender(func) {
    sender = func;
}
exports.setSender = setSender;
function* packetReceiver() {
    while (true) {
        let cmd = yield;
        //console.log(cmd >> 5);
        if (cmd >> 5 == 0x04) {
            let pin = (cmd & 0x03) >> 2;
            let value = (cmd & 0x03) + (yield);
            console.log("Voltage @" + pin + " = " + value);
            exports.analogValues[pin] = value;
        }
        else if (cmd >> 5 == 0x02) {
        }
    }
}
exports.receiver = packetReceiver();
exports.receiver.next();
// Commands
var PIN_TYPE;
(function (PIN_TYPE) {
    PIN_TYPE[PIN_TYPE["DIGITAL"] = 0] = "DIGITAL";
    PIN_TYPE[PIN_TYPE["ANALOG"] = 1] = "ANALOG";
})(PIN_TYPE = exports.PIN_TYPE || (exports.PIN_TYPE = {}));
var PIN_MODE;
(function (PIN_MODE) {
    PIN_MODE[PIN_MODE["NONE"] = 2] = "NONE";
    PIN_MODE[PIN_MODE["INPUT"] = 3] = "INPUT";
    PIN_MODE[PIN_MODE["INPUT_PULLUP"] = 0] = "INPUT_PULLUP";
    PIN_MODE[PIN_MODE["OUTPUT"] = 1] = "OUTPUT";
    PIN_MODE[PIN_MODE["OFF"] = 0] = "OFF";
    PIN_MODE[PIN_MODE["ON"] = 1] = "ON";
})(PIN_MODE = exports.PIN_MODE || (exports.PIN_MODE = {}));
function ultraSonicBegin(pingPin, echoPin, rate) {
    rate = rate || 1;
    if (pingPin < 8 && echoPin < 8) {
        sender([0x10, (pingPin << 4) + echoPin, rate - 1]); // 00 01 00 00, pp pp ee ee, rr rr rr rr
    }
    else {
        sender([0x11, pingPin, echoPin, rate - 1]); // 00 01 00 01, pp pp pp pp, ee ee ee ee, rr rr rr rr
    }
}
exports.ultraSonicBegin = ultraSonicBegin;
function pinMode(pinType, pin, pinMode) {
    pinMode = pinMode === undefined ? PIN_MODE.ON : pinMode;
    if (pin < 8 && pinMode <= 1) {
        sender([0xC0 + (pinType << 4) + (pin << 1) + pinMode]); // 11 0t pp pm
    }
    else {
        sender([0xE0 + (pinType << 4) + (pin >> 6), ((pin & 0xFC) << 2) + pinMode]); // 11 1t pp pp, pp pp pp mm
    }
}
exports.pinMode = pinMode;
function digitalWrite(pin, value) {
    if (pin < 16) {
        sender([(0x40) + (pin << 1) + (value & 0x01)]); // 01 0p pp pv
    }
    else {
        // TODO
        sender([(0x60) + (pin >> 6), (pin << 2) + (value & 0x01)]); // 01 10 pp pp, pp pp pp 0v
    }
}
exports.digitalWrite = digitalWrite;
function reset() {
    sender([0x70]);
}
exports.reset = reset;
// Variables
let ainValues = [];
ainValues.length;
//////////////////
function test() {
    //reset();
    /*setInterval(() => {
        digitalWrite(0, 1);
        setTimeout(() => {
            digitalWrite(0, 0);
        }, 500);
    }, 1000);*/
    pinMode(PIN_TYPE.ANALOG, 5, PIN_MODE.ON);
    //ultraSonicBegin(0, 1, 10);
}
exports.test = test;
//# sourceMappingURL=libsensorboard.js.map