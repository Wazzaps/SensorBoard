# Experiment File format
```
sensors: {
    sensor type: 1 byte
    sensor name: ??? bytes (utf8, null terminated)
    sensor args: ??? bytes (dependant on sensor type)
}

experiment file: sensors[]
```

# Log file format
```
result: {
    timestamp: 6 bytes
    sensor results: ??? bytes (dependant on sensor type)
}
log file: {
    experiment file: experiment file
    null terminator: 1 byte = 0x00
    results: result[]
}
```

# Example log file
```
log file: {
    experiment file = [
        { 3, "Distance from object", 1, 2 }
    ] (1+21+1+1 = 24 bytes)
    null terminator = 0x00
    results = [
        { 463287324, 1000 }
        { 463287356, 1010 }
        ...(100 times total)
        { 463326381,  990 }
    ] ((6+2)*100 = 800 bytes)
} (total 852 bytes)
```

# Sensors
## Digital input (1)
### Log file format
`state: byte`
### Pins
`pin: byte`

## Analog input (2)
### Log file format
`voltage: 2 bytes`
### Pins
`pin: byte`

## Ultrasonic (3)
### Log file format
`distance: 2 bytes`
### Pins
`echoPin: byte`  
`pingPin: byte`