"use strict";
//const btSerial = new (require('bluetooth-serial-port')).BluetoothSerialPort();
Object.defineProperty(exports, "__esModule", { value: true });
var bluetooth = require('node-bluetooth');
var firmata = require('firmata');
var child_process = require("child_process");
var firmataBoard;
var isFirmataConnected = true;
var devicesFound = [];
setInterval(function () {
    var output = child_process.execSync("bt-device -l", { timeout: 100, encoding: "utf8" })
        .split("\n");
    output.shift();
    output.pop();
    output = output.map(function (dev) { return dev.split(" ("); });
    output = output.map(function (dev) { return ({
        name: dev[0],
        bt_id: dev[1].slice(0, -1)
    }); });
    devicesFound = output;
}, 1000);
// close the connection when you're ready
/*btSerial.on('found', (address, name) => {
    console.log(name, address);
    btSerial.findSerialPortChannel(address, channel => {
        console.log(name, address, channel);
        if (finishedScan) {
            devicesFound = [];
            finishedScan = false;
        }
        devicesFound.push({
            name,
            bt_id: address,
            channel: channel
        });
        connectToBluetoothDevice(address);
    }, function() {
        console.log('found nothing');
    });
});

btSerial.on("finished", () => {
    console.log('finished');
    finishedScan = true;
});

btSerial.inquire();*/
//setInterval(() => btSerial.inquire(), 10000);
function findBluetoothDevices() {
    return devicesFound;
}
exports.findBluetoothDevices = findBluetoothDevices;
function getBoard() {
    return isFirmataConnected ? firmataBoard : null;
}
exports.getBoard = getBoard;
function connectToBluetoothDevice(bt_id) {
    bluetooth.connect(bt_id, 1, function (err, connection) {
        if (err)
            return console.error(err);
        firmataBoard = new firmata(connection, function () {
            isFirmataConnected = true;
            firmataBoard.pins[13].mode = 1;
            console.log("connected2");
        });
        console.log("connected");
    });
}
exports.connectToBluetoothDevice = connectToBluetoothDevice;
function disconnectToBluetoothDevice() {
    bluetooth;
}
exports.disconnectToBluetoothDevice = disconnectToBluetoothDevice;
