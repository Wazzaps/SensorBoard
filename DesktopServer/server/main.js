"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var devicemanager_1 = require("./devicemanager");
var newtons_first_law_1 = require("./newtons_first_law");
var app = express();
app.use(express.static("client/dist"));
newtons_first_law_1.restifyArrayOfObjects(app, '/api/devices', devicemanager_1.findBluetoothDevices);
app.get("/api/digitalWrite/:pin/:to", function (req, res) {
    devicemanager_1.getBoard().digitalWrite(parseInt(req.params.pin), parseInt(req.params.to));
    res.end("OK");
});
app.get("/api/connect/:bt_id", function (req, res) {
    console.log("Connecting to", req.params.bt_id);
    devicemanager_1.connectToBluetoothDevice(req.params.bt_id);
    res.end("OK");
});
app.get("/api/disconnect", function (req, res) {
    console.log("Disconnecting from device");
    disconnectToBluetoothDevice();
    res.end("OK");
});
app.listen(19123, "localhost", function () { return console.log('SensorBoard server listening on port 19123'); });
