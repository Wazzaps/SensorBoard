//const btSerial = new (require('bluetooth-serial-port')).BluetoothSerialPort();

const bluetooth = require('node-bluetooth');
const firmata = require('firmata');
const repl = require('repl');
import * as child_process from "child_process";

let btConnection;
let firmataBoard;
let isBTConnected = false
let isFirmataConnected = false;

export interface FoundDevice {
	name: string;
	bt_id: string;
}

let devicesFound: FoundDevice[] = [];

setInterval(() => {
	let output: any = child_process.execSync("bt-device -l", { timeout: 100, encoding: "utf8" })
		.split("\n");
	output.shift();
	output.pop();
	output = output.map(dev => dev.split(" ("));
	output = output.map((dev): FoundDevice => ({
		name: dev[0],
		bt_id: dev[1].slice(0, -1)
	}));
	devicesFound = output;
}, 1000);

// close the connection when you're ready
/*btSerial.on('found', (address, name) => {
	console.log(name, address);
	btSerial.findSerialPortChannel(address, channel => {
		console.log(name, address, channel);
		if (finishedScan) {
			devicesFound = [];
			finishedScan = false;
		}
		devicesFound.push({
			name,
			bt_id: address,
			channel: channel
		});
		connectToBluetoothDevice(address);
	}, function() {
		console.log('found nothing');
	});
});

btSerial.on("finished", () => {
	console.log('finished');
	finishedScan = true;
});

btSerial.inquire();*/
//setInterval(() => btSerial.inquire(), 10000);


export function findBluetoothDevices(): FoundDevice[] {
	return devicesFound;
}

export function getBoard() {
	return isFirmataConnected ? firmataBoard : null;
}

export function connectToBluetoothDevice(bt_id: string) {
	bluetooth.connect(bt_id, 1, function(err, connection){
		if(err) return console.error(err);
		isBTConnected = true;
		btConnection = connection;
		firmataBoard = new firmata(connection, () => {
			isFirmataConnected = true;
			firmataBoard.pins[13].mode = 1;
			console.log("Connected to Firmata Service");
			repl.start("> ").context.brd = firmataBoard;
		});
		console.log("Connected to Bluetooth device");
	});
}

export function cmdSetTime(hr: number, min: number, sec: number, day: number, month: number, yr: number) {
	firmataBoard.sysexCommand([0x01, hr, min, sec, day, month, yr].map(v => v & 0x7F));
}

export function cmdUploadExperiment() {
	firmataBoard.sysexCommand([0x02]);

}

export function cmdExecExperiment() {
	firmataBoard.sysexCommand([0x03]);
}

export function cmdListLogs() {
	firmataBoard.sysexCommand([0x04]);

}

export function cmdDownloadLog() {
	firmataBoard.sysexCommand([0x05]);

}

export function disconnectFromBluetoothDevice() {
	btConnection.close()
}