//const btSerial = new (require('bluetooth-serial-port')).BluetoothSerialPort();

const bluetooth = require('node-bluetooth');
//const repl = require('repl');
import * as child_process from "child_process";

let btConnection;
//let firmataBoard;
let isBTConnected = false;
//let isFirmataConnected = false;

export interface FoundDevice {
	name: string;
	bt_id: string;
}


export function findBluetoothDevices(): FoundDevice[] {
	let output: any = child_process.execSync("bt-device -l", { timeout: 100, encoding: "utf8" })
		.split("\n");
	output.shift();
	output.pop();
	output = output.map(dev => dev.split(" ("));
	output = output.map((dev): FoundDevice => ({
		name: dev[0],
		bt_id: dev[1].slice(0, -1)
	}));
	return output;
}

export function getBoard() {
	return isBTConnected ? btConnection : null;
}

export function connectToBluetoothDevice(bt_id: string, callback: (conn) => void) {
	bluetooth.connect(bt_id, 1, function(err, connection){
		if(err) return console.error(err);
		isBTConnected = true;
		btConnection = connection;
		callback(connection);
		console.log("Connected to Bluetooth device");
	});
}

export function disconnectFromBluetoothDevice() {
	btConnection.close()
}